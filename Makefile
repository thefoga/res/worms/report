cwd            := $(shell pwd)

tex_compile    := pdflatex
tex_file       := main
bib_make       := biber

.PHONY: all view

clean::
	rm -fv *.aux *.log *.bbl *.blg *.toc *.out *.lot *.lof *.atoc
	rm -fv *.bcf *.glo *.ist *.run.xml *-blx.bib *.acn *.synctex.gz
	rm -fv */*.aux */*.log */*.bbl */*.blg */*.toc */*.out */*.lot */*.lof */*.atoc
	rm -fv */*.bcf */*.glo */*.ist */*.run.xml */*-blx.bib */*.acn */*.synctex.gz
	rm -fv */*/*.aux */*/*.log */*/*.bbl */*/*.blg */*/*.toc */*/*.out */*/*.lot */*/*.lof */*/*.atoc
	rm -fv */*/*.bcf */*/*.glo */*/*.ist */*/*.run.xml */*/*-blx.bib */*/*.acn */*/*.synctex.gz

cleanall::
	$(MAKE) clean
	rm -fv *.pdf

view::
	xdg-open $(tex_file).pdf

fast::
	$(tex_compile) $(tex_file)

build::
	$(tex_compile) $(tex_file)
	$(bib_make) $(tex_file)
	$(tex_compile) $(tex_file)
	$(tex_compile) $(tex_file)

rebuild::
	$(MAKE) cleanall
	$(MAKE) build

debug::
	$(MAKE) rebuild
	$(MAKE) view
