\section{Mapping 2}
\label{sec:m2}

\subsection{Framework}
\label{subsec:m2_framework}

\textit{pix2pix} GAN is the umbrella term (and also an algorithm, see \cite{isola2018imagetoimage}) describing GAN-based methods to convert and image into another under some constraints.

In this framework

\begin{itemize}
    \item \textbf{content} is the actual content of the image that is to be converted into the other image. In this case it's the worm.
    \item \textbf{style} is the texture, painting style, noise ... that are required to appear in the converted image. In this case it's the low-quality style of LQs.
\end{itemize}

\subsection{Literature}
\label{subsec:m2_literature}

As expected, there are tons of approaches to follow, and the usual one is \cite{zhu2020unpaired}; \cite{shaham2019singan} should also be noted, even if it does not really solve this problem.

However \foga{} went with \cite{park2020contrastive}'s model, the reasons being

\begin{itemize}
    \item they are mainly the same authors and it's a more recent publication solving the same problem thus it must have something more
    \item \cite{park2020contrastive} seems to really have something more (see Experiments section)
    \item code base is highly modular, thus extending it would be not much stress
    \item code base includes other models that can be tried (there is \cite{zhu2020unpaired}'s as well)
\end{itemize}

\cite{park2020contrastive} defines the following terms when dealing with the training dataset

\begin{itemize}
    \item $\mathcal{A}$ image domain corresponds to HQs
    \item $\mathcal{B}$ image domain corresponds to LQs
    \item \textit{fake} and \textit{real} denote respectively a GAN-generated image and a real one
    \item \texttt{real\_A} is a HQ input to the GAN (during inference phase)
    \item \texttt{real\_B} is \textit{any} (due to \Cref{iss:hq_lq_timing}) LQ
    \item \texttt{fake\_B} is the output of the GAN: hopefully it resembles (content-wise) \texttt{real\_A} and mirrors \texttt{real\_B}'s style
\end{itemize}

\subsection{Experiments}
\label{subsec:m2_experiments}

All experiments were run in the \textit{ml} partition of \href{https://tu-dresden.de/zih/hochleistungsrechnen/hpc}{Taurus} in a \href{https://doc.zih.tu-dresden.de/hpc-wiki/bin/view/Compendium/JupyterHub#Creating_and_using_your_own_environment}{custom Conda environment}. Runtime was $\sim 2$ hours for $400$ training epochs with $\sim 80$ training images on a single \href{https://doc.zih.tu-dresden.de/hpc-wiki/bin/view/Compendium/HPCDA}{NVIDIA Tesla V100}. \\
Each experiment is assigned a unique \textit{ID} in order to reference if in the results table.

\subsection{First batch of experiments}
\label{subsubsec:m2_first_experiments_results}

\tab[m2_first_experiments_setup]{} shows each experiment setup while \tab[m2_first_experiments_results]{} shows the results. Sampled outputs can be seen in \Cref{fig:m2_first_experiments_samples}.

\begin{table}[h]
    \centering
    \rowcolors{2}{black!10}{black!40}
    \begin{tabular}{*3l} \toprule
    
    ID & $\lambda_\text{GAN}$ & $\lambda_\text{PatchNCE}$ \\ \midrule
    
    1 & 1.0 & 1.0 \\
    2 & 1.0 & 2.0 \\
    3 & 1.0 & 10.0 \\
    4 & 1.0 & 100.0 \\
    
    \bottomrule
    \hline
    \end{tabular}
    \caption{Experiments setup}
    \label{tab:m2_first_experiments_setup}
\end{table}

where

\begin{itemize}
    \item $\lambda_\text{GAN}$ is the weight of $\mathcal{L}_\text{GAN}$
    \item $\lambda_\text{PatchNCE}$ is the weight of $\mathcal{L}_\text{PatchNCE}(G, H, X)$ and $\mathcal{L}_\text{PatchNCE}(G, H, Y)$
\end{itemize}

\nb{} the final PatchNCE loss in \cite{park2020contrastive} is

\begin{equation}
    \frac{ \lambda_\text{PatchNCE} \cdot ( \mathcal{L}_\text{PatchNCE}(G, H, X) + \mathcal{L}_\text{PatchNCE}(G, H, Y) )}{2}
\end{equation}

\begin{table}[h]
    \centering
    \rowcolors{2}{black!10}{black!40}
    \begin{tabular}{*5l} \toprule
    
    ID & $C$ & $S$ & description & why it happens \\ \midrule

    1 & \xmark{} & \cmark{} & deformations (see \Cref{iss:gan_deform} ) & \\
    2 & \xmark{} & \cmark{} & \textit{blob} does not follow HQ \textit{blob} & \\
    3 & \xmark{} & \cmark{} & too much \textit{style} & \\
    4 & \cmark{} & \xmark{} & \textit{milky} (see \Cref{iss:milk} ) & \Cref{supp:milk} \\
    
    \bottomrule
    \hline
    \end{tabular}
    \caption{Experiments results}
    \label{tab:m2_first_experiments_results}
\end{table}

where

\begin{itemize}
    \item $C$ denotes the quality of the output content
    \item $S$ denotes the quality of the output content
\end{itemize}

\subsubsection{Issues}
\label{subsubsec:m2_first_issues}

\begin{issue}
\label{iss:blob}
    \fg[m2_first_experiments_samples_0_fake_B]{} and \fg[m2_first_experiments_samples_1_fake_B]{} show this issue very well: the big high-intensity \textit{blob} disappears (cit.)
\end{issue}

\begin{issue}
\label{iss:gan_deform}
    With reference to \fg[m2_first_experiments_samples_3_fake_B]{} one clearly sees that the output does not keep the same geometrical structure of the input
\end{issue}

\begin{issue}
\label{iss:milk}
    Some of the generated images contain a \textit{milky} (cit.) style, like in \fg[m2_second_experiments_samples_0_fake_B]{}.
\end{issue}

\subsection{Second batch of experiments}
\label{subsec:m2_second_experiments}

\subsubsection{Setup}
\label{subsubsec:m2_second_experiments_setup}

Following \core{}'s suggestion varying (linearly) weights were introduced in calculating $\mathcal{L}_\text{PatchNCE}$: all experiments in this batch contain a new parameter $k$ regulating how much weights would vary.

Moreover $\mathcal{L}_\text{PatchNCE}(G, H, X)$ had a different weight than $\mathcal{L}_\text{PatchNCE}(G, H, Y)$, thus splitting neatly the $2$ loss components.

Post processing was introduced in the last experiment: Gaussian noise was added to the GAN output in order to mirror LQs histogram.

\subsubsection{Results}
\label{subsubsec:m2_second_experiments_results}

\tab[m2_second_experiments_setup]{} shows each experiment setup while \tab[m2_second_experiments_results]{} shows the results. Sampled outputs can be seen in \Cref{fig:m2_second_experiments_samples}.

\begin{table}[h]
    \centering
    \rowcolors{2}{black!10}{black!40}
    \begin{tabular}{*5l} \toprule
    
    ID & $w$ & $\lambda_\text{GAN}$ & $\lambda_X$ & $\lambda_Y$ \\ \midrule
    
    1 & 1.2 & 1.0 & 10.0 & 10.0 \\
    2 & 1.4 & 1.0 & 20.0 & 5.0 \\
    3 & 1.4 & 1.0 & 20.0 & 10.0 \\
    4 & 1.5 & 1.0 & 20.0 & 2.0 \\
    5 & 1.5 & 1.0 & 10.0 & 5.0 \\
    6 & 1.8 & 1.0 & 100.0 & 1.0 \\
    
    \bottomrule
    \hline
    \end{tabular}
    \caption{Experiments setup}
    \label{tab:m2_second_experiments_setup}
\end{table}

where

\begin{itemize}
    \item $\lambda_\text{GAN}$ is the weight of $\mathcal{L}_\text{GAN}$
    \item $\lambda_X$ is the weight of $\mathcal{L}_\text{PatchNCE}(G, H, X)$
    \item $\lambda_Y$ is the weight of $\mathcal{L}_\text{PatchNCE}(G, H, Y)$
    \item $w$ is the ratio between the biggest and smallest weight when calculating $\mathcal{L}_\text{PatchNCE}$. \nb{} the mean of the weights is $1.0$
\end{itemize}

\nb{} the final PatchNCE loss is

\begin{equation}
    \frac{ \lambda_X \cdot w \cdot \mathcal{L}_\text{PatchNCE}(G, H, X) + \lambda_Y \cdot w \cdot \mathcal{L}_\text{PatchNCE}(G, H, Y)}{2}
\end{equation}

\begin{table}[h]
    \centering
    \rowcolors{2}{black!10}{black!40}
    \begin{tabular}{*5l} \toprule
    
    ID & $C$ & $S$ & description & why it happens \\ \midrule

    1 & $\sim$ & \cmark{} & too much \textit{style} & \\
    2 & $\sim$ \cmark{} & \cmark{} & sometimes not \textit{milky} & \\
    3 & \xmark{} & \cmark{} & too much \textit{style}, not \textit{milky} & \\
    4 & \xmark{} & \cmark{} & too much \textit{style} + deformations (see \Cref{iss:gan_deform} ) & \\
    5 & \xmark{} & \cmark{} & too much \textit{style} & \\
    6 & \cmark{} & \cmark{} (if post-processed) & \textit{milky} (see \Cref{iss:milk} ) & \Cref{supp:milk} \\
    
    \bottomrule
    \hline
    \end{tabular}
    \caption{Experiments results}
    \label{tab:m2_second_experiments_results}
\end{table}

\begin{supp}
\label{supp:milk}
    \foga{} thinks \Cref{iss:milk} that the varying weights in calculating $\mathcal{L}_\text{PatchNCE}$ is the reason why: giving more weight to the deeper layers means the GAN learns more abstract features rather than pixel-wise features per patch. However \Cref{iss:milk} happens even if weights do not change (but there is a lot of weight in $\mathcal{L}_\text{PatchNCE}$).
\end{supp}

\begin{issue}
\label{iss:histogram}
    Clearly, as seen in \fg[m2_second_experiments_samples_3_post_processed]{} the histogram of the post-processed image does not match the histogram of a LQ
\end{issue}

\subsection{Future work}
\label{subsec:m2_future}

\subsubsection{Better noising}
\label{subsubsec:m2_future_noise}

\core{} suggested convolution of input image (\ie{} HQ) with Gaussian could improve results. Moreover Poisson noise should be applied in post-processing. Moreover, mirroring the power spectrum of LQs could be improving post-processing.

\subsubsection{Patchwise loss on CycleGAN}
\label{subsubsec:m2_future_noise}

\lumi{} suggested applying $\mathcal{L}_\text{PatchNCE}$ on \cite{zhu2020unpaired}
